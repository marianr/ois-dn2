var Klepet = function(socket) {
  this.socket = socket;
  this.bannedWords = [];
  
  var self = this;
  $.get('swearWords.txt', function(data) {
    self.bannedWords = data.toString().split("\r\n");
  });
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, password) {
  password = password || '';
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    password: password
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal;
      var password;
      
      var phrase = besede.join(' ');
      var phraseLength = phrase.length;
      // split to room name and password
      var split = phrase.substring(1, phraseLength - 1).split(/"\s+"/, 2);
      
      // password specified
      if (split.length === 2) {
        if (phrase.charAt(0) !== '"' || phrase.charAt(phraseLength - 1) !== '"') {
          sporocilo = 'Invalid command.';
          break;
        }
        
        kanal = split[0];
        password = split[1];
        
      } else {
        kanal = besede.join(' ');
      }
      
      this.spremeniKanal(kanal, password);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var phrase = besede.join(' ');
      var phraseLength = phrase.length;
      // split to recipent and message
      var split = phrase.substring(1, phraseLength - 1).split(/"\s+"/, 2);
      
      if (phrase.charAt(0) !== '"' || phrase.charAt(phraseLength - 1) !== '"' || split.length !== 2) {
        sporocilo = 'Invalid command.';
        break;
      }
      
      var recipent = split[0];
      var message = split[1];
      this.socket.emit('privateMessage', {
        recipent: recipent,
        message: message
      });
      sporocilo = 'Private message to "' + recipent + '": ' + message;
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

Klepet.prototype.filterBannedWords = function (text) {
  for (var i = 0, len = this.bannedWords.length; i < len; i++) {
    var word = this.bannedWords[i];
    var stars = '';
  
    for (var j = 0, starsNum = word.length; j < starsNum; j++) {
      stars += '*';
    }
    var pattern = '\\b' + this.bannedWords[i] + '\\b';
    text = text.replace(new RegExp(pattern, 'gi'), stars);
  }
  return text;
};
