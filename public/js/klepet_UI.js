function divElementEnostavniTekst(sporocilo) {
  var elem = $('<div style="font-weight: bold"></div>');
  var escaped = elem.text(sporocilo).html();
  var textWithSmilies = processSmilies(escaped);
  return elem.html(textWithSmilies);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = klepetApp.filterBannedWords(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function processSmilies(text) {
  var smilies = {
    ';\\)': 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png',
    ':\\)': 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png',
    '\\(y\\)': 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png',
    ':\\*': 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png',
    ':\\(': 'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png',
  };
  
  for (var smile in smilies) {
    text = text.replace(new RegExp(smile, 'gi'), '<img src="' + smilies[smile] +'"/>');
  }
  return text;
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      // display nickname in header
      $('#nickname').text(rezultat.vzdevek + ' @ ');
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var message = klepetApp.filterBannedWords(sporocilo.besedilo);
    var novElement = divElementEnostavniTekst(message);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('userList', function (userList) {
    var elems = [];
    for (var i in userList) {
      elems.push($('<div></div>').text(userList[i]));
    }
    
    $('#room-users').html(elems);
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});