var socketio = require('socket.io');
var fs  = require('fs');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var roomPasswords = {};

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    handlePrivateMessage(socket, io.sockets, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    
    socket.on('disconnect', function() {
      var room = trenutniKanal[socket.id];
      delete trenutniKanal[socket.id];
      
      socket.leave(room);
      
      socket.broadcast.to(room).emit('sporocilo', {
        besedilo: vzdevkiGledeNaSocket[socket.id] + ' has left the room ' + room + '.'
      });
      socket.broadcast.to(room).emit('userList', getRoomUsers(room));
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var roomUsers = getRoomUsers(kanal);
  var otherRoomUsers = [];
  
  for (var i in roomUsers) {
    if (i != socket.id) {
      otherRoomUsers.push(vzdevkiGledeNaSocket[i]);
    }
  }
  
  if (otherRoomUsers.length > 0) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    uporabnikiNaKanaluPovzetek += otherRoomUsers.join(', ') + '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
  
  broadcastUserList(socket, kanal, true);
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        var room = trenutniKanal[socket.id];
        
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        
        socket.broadcast.to(room).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
        
        broadcastUserList(socket, room, true);
        
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function handlePrivateMessage(socket, sockets, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('privateMessage', function (data) {
    var recipent = data.recipent;
    var message = data.message;
    var sendErrorBack = function () {
      socket.emit('sporocilo', {
        besedilo: 'Message "' + message + '" could not be sent to a user "' + recipent + '"'
      });
    };
    
    if (uporabljeniVzdevki.indexOf(recipent) === -1) {
      // username not found
      sendErrorBack();
      return;
    }
    
    for (var id in vzdevkiGledeNaSocket) {
      if (vzdevkiGledeNaSocket[id] === recipent) {
        sockets.socket(id).emit('sporocilo', {
          besedilo: vzdevkiGledeNaSocket[socket.id] + ' (private): ' + message
        });
        return;
      }
    }
    
    // username not found
    sendErrorBack();
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var oldRoomName = trenutniKanal[socket.id];
    var newRoomName = kanal.novKanal;
  
    var valid = checkRoomPassword(newRoomName, kanal.password);
    if (!valid) {
      socket.emit('sporocilo', {
        besedilo: 'Joining the channel "' + newRoomName + '" was not successfull due to wrong password!'
      });
      return;
    }
    
    socket.leave(oldRoomName);
    cleanRoomPasswordIfEmpty(oldRoomName);
    broadcastUserList(socket, oldRoomName);
    pridruzitevKanalu(socket, newRoomName);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}

function getRoomUsers(room) {
  var uporabnikiNaKanalu = io.sockets.clients(room);
  var roomUsers = {};
  
  for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      var userName = vzdevkiGledeNaSocket[uporabnikSocketId];
      
      roomUsers[uporabnikSocketId] = userName;
  }
  return roomUsers;
}

function broadcastUserList(socket, room, includeSender) {
  var roomUsers = getRoomUsers(room);
  
  if (includeSender) {
    socket.emit('userList', roomUsers);
  }
  socket.broadcast.to(room).emit('userList', roomUsers);
}

function checkRoomPassword(room, password) {
  var roomUsersCount = io.sockets.clients(room).length;
  
  if (roomPasswords[room] && roomUsersCount > 0) {
    // check existing password
    if (password !== roomPasswords[room]) {
      return false;
    }
    
  } else if (password && roomUsersCount <= 0) {
    // set new password
    roomPasswords[room] = password;
  }
  
  return true;
}

function cleanRoomPasswordIfEmpty(room) {
  if (roomPasswords[room] && io.sockets.clients(room).length <= 0) {
    // clean unused memory
    delete roomPasswords[room];
  }
}
